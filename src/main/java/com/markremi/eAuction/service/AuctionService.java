package com.markremi.eAuction.service;

import com.markremi.eAuction.model.AuctionEvent;
import com.markremi.eAuction.model.User;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Logger;

@Component
public class AuctionService {

    private final static Logger LOGGER = Logger.getLogger(AuctionService.class.getName());

    // This is a class level map, we are absolutely making the assumption that this is a singleton class.
    // Using regular hash map instead of concurrentHash map.
    Map<String, Timer> eventsAndTimers = new HashMap<>();

    /**
     * Starts auction, sets auction start time.
     *
     * @param event - Event to start.
     */
    public void startAuction(AuctionEvent event) {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {
                event.setEnd(LocalDateTime.now());
            }
        };

        // Setting up the timer to cancel when time runs out. By "Cancel" we mean set the "End Time".
        Timer timer = new Timer("Timer");
        Duration timeToLive = Duration.between(event.getStart(), event.getEnd());
        timer.schedule(task, timeToLive.getSeconds() * 1000);

        // Set the event start time & add event/timer to our map.
        event.setStart(LocalDateTime.now());
        eventsAndTimers.put(event.getId(), timer);
    }

    /**
     * Stops auction, sets final auction end time.
     *
     * @param event - Event to stop.
     */
    public void endAuction(AuctionEvent event) {
        // Prematurely ended the auction, let's find that timer and stop it manually.
        Optional<Timer> timer = Optional.ofNullable(eventsAndTimers.get(event.getId()));

        // Taking care of our nulls.
        if (timer.isPresent()) {
            eventsAndTimers.get(event.getId()).cancel();
            event.setEnd(LocalDateTime.now());
        } else {
            // Due to time, I'm not adding fancy formatters here, just going to concatenate strings.
            LOGGER.severe("Couldn't find auction " + event.getName() + " to cancel!");
        }

        // Due to time, I'm not adding fancy formatters here, just going to concatenate strings.
        LOGGER.info("Stopped auction prematurely for " + event.getName());
    }

    /**
     * Only the highest bid is persisted. Lower bids are ignored.
     *
     * @param event - Bidding event.
     * @param user  - Bidder.
     * @param offer - User's bid.
     */
    public void placeBid(AuctionEvent event, User user, double offer) {

        // Keeping track of our bidders!
        event.getBidders().add(user);

        // Only the highest bid matters.
        if (offer > event.getHighestBid()) {
            event.setHighestBid(offer);
            event.setAuctionWinner(user);
        }
    }
}
