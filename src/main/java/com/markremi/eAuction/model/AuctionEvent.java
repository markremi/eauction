package com.markremi.eAuction.model;

import javax.persistence.Entity;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Entity
public class AuctionEvent {

    // Based on requirements, we are assuming a 1 to 1 relationship between Auction (the event)
    // and Product (the item being sold).
    private Product product;
    private List<User> bidders = new ArrayList<>();
    private double highestBid;
    private User auctionWinner;
    private double startingValue;
    private String id;
    private String name;
    private LocalDateTime start;
    private LocalDateTime end;

    public AuctionEvent(String name, Product product, double startingValue, LocalDateTime end) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.startingValue = startingValue;
        this.highestBid = startingValue;
        this.product = product;
        this.setStart(LocalDateTime.now()); // When you create the event, we are ready to bid!
        this.setEnd(end);
    }

    public List<User> getBidders() {
        return bidders;
    }

    public void setBidders(List<User> bidders) {
        this.bidders = bidders;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getHighestBid() {
        return highestBid;
    }

    public void setHighestBid(double highestBid) {
        this.highestBid = highestBid;
    }

    public User getAuctionWinner() {
        return auctionWinner;
    }

    public void setAuctionWinner(User auctionWinner) {
        this.auctionWinner = auctionWinner;
    }

    public double getStartingValue() {
        return startingValue;
    }

    public void setStartingValue(double startingValue) {
        this.startingValue = startingValue;
    }

    public LocalDateTime getStart() {
        return start;
    }

    public void setStart(LocalDateTime start) {
        this.start = start;
    }

    public LocalDateTime getEnd() {
        return end;
    }

    public void setEnd(LocalDateTime end) {
        this.end = end;
    }
}
