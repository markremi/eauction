package com.markremi.eAuction;

import com.markremi.eAuction.model.AuctionEvent;
import com.markremi.eAuction.model.Product;
import com.markremi.eAuction.model.User;
import com.markremi.eAuction.service.AuctionService;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDateTime;
import java.util.logging.Level;
import java.util.logging.Logger;

@SpringBootApplication
public class EAuctionApplication {

    private final static Logger LOGGER = Logger.getLogger(EAuctionApplication.class.getName());

	/*
	REQUIREMENTS:
		Please implement the back-end for a simple eBay style auction in the language of your choice. Some requirements:
		-The implementation should include a data model that supports multiple auctions, users, and multiple bids per auction, at a minimum.
		-Auctions should be time-limited.
		-Include basic business logic to handle starting an auction, placing a bid, and ending an auction.
		-Where requirements are not explicitly stated or clear, use your best judgment and state any assumptions made.
		-The finished product should compile, but it doesn't have to be executable or in any kind of final form.
		-Please don't spend more than a couple of hours on this. The intent isn't for perfection, but rather to give an example of your coding style, design, and proficiency.
	 */

	public static void main(String[] args) {
		SpringApplication.run(EAuctionApplication.class, args);

		// NOTE: I've added the logs to README.md

		// Our products (items for sale)
        Product mySweetAxe = new Product("Mark's Guitar",
                "1969 Les Paul. Re-live the golden era!");

        Product stopSign = new Product("Octagon Stop Sign",
                "I didn't steal this, but it's for sale! Legit.");

        Product grandFatherClock = new Product("Old School Clock",
                "Family heirloom. No scratches, still keeps good time!");

        Product fakeArt = new Product("Zona Lisa",
                "It's not an original Da Vinci, I painted it. Show me the money.");

        // Our bidders.
        User mark = new User("Mark", "Remi");
        User brianLenz = new User("Brian", "Lenz");
        User tedONeill = new User("Ted", "O'Neill");
        User billCawthra = new User("Bill", "Cawthra");

        // Our auctions (currently just one item per auction due to my time constraint)
        AuctionEvent event1 = new AuctionEvent("Rock n roll ain't noise pollution", mySweetAxe, 350.00, LocalDateTime.now().plusMinutes(5));
        AuctionEvent event2 = new AuctionEvent("STOP drop and roll", stopSign, 15.00, LocalDateTime.now().plusHours(1));
        AuctionEvent event3 = new AuctionEvent("Time is ticking away.", grandFatherClock, 2488.99, LocalDateTime.now().plusMinutes(42));
        AuctionEvent event4 = new AuctionEvent("Look good for cheap!", fakeArt, 25_000.00, LocalDateTime.now().plusDays(2));

        // Could autowire this in a real implementation, for now, we'll just initialize the service here...
        AuctionService auctionService = new AuctionService();
        auctionService.startAuction(event1);
        auctionService.placeBid(event1, mark, 200);
        auctionService.placeBid(event1, brianLenz, 550);
        auctionService.placeBid(event1, brianLenz, 351);

        // Notice event 2 has no bids at all. This is intentional to test a "no bid" scenario. And... no one wants a
        // stolen Stop sign.

        auctionService.startAuction(event3);
        auctionService.placeBid(event3, tedONeill, 2600.99);
        auctionService.placeBid(event3, billCawthra, 5200);
        auctionService.placeBid(event3, brianLenz, 3523.12);

        auctionService.startAuction(event4);
        auctionService.placeBid(event4, mark, 34_000.32);

        auctionService.endAuction(event1);
        auctionService.endAuction(event2); // This is safely caught, we never started auction for event 2. You'll see a 'severe' message in the logs.
        auctionService.endAuction(event3);
        auctionService.endAuction(event4);

        // Lets print out the raw results. I'm using this for testing and will leave it here for completeness.
        LOGGER.setLevel(Level.INFO);
        LOGGER.info(ReflectionToStringBuilder.toString(event1));
        LOGGER.info(ReflectionToStringBuilder.toString(event3));
        LOGGER.info(ReflectionToStringBuilder.toString(event4));

        // You'll notice "event2" has a null highest bidder and it's starting bid = highest bid.
        LOGGER.info(ReflectionToStringBuilder.toString(event2));

        // Looks like I have some time, I'm going to pretty-print the results. I've added this to the README.md as well.
        LOGGER.info("............"); // Giving ourselves some space for legibility.
        LOGGER.info("............");
        LOGGER.info("............");
        LOGGER.info("Auction \"" + event1.getName() + "\": Starting price: $" + event1.getStartingValue() + ", Winning Bid : $" + event1.getHighestBid() + ", Highest Bidder: " + event1.getAuctionWinner().getFirstName() + " " + event1.getAuctionWinner().getLastName() + ", All the bidders: " + event1.getBidders().toString());
        LOGGER.info("Auction \"" + event2.getName() + "\": Starting price: $" + event2.getStartingValue() + ", Winning Bid : $" + event2.getHighestBid() + ", Highest Bidder: " + event2.getAuctionWinner());
        LOGGER.info("Auction \"" + event3.getName() + "\": Starting price: $" + event3.getStartingValue() + ", Winning Bid : $" + event3.getHighestBid() + ", Highest Bidder: " + event3.getAuctionWinner().getFirstName() + " " + event3.getAuctionWinner().getLastName() + ", All the bidders: " + event3.getBidders().toString());
        LOGGER.info("Auction \"" + event4.getName() + "\": Starting price: $" + event4.getStartingValue() + ", Winning Bid : $" + event4.getHighestBid() + ", Highest Bidder: " + event4.getAuctionWinner().getFirstName() + " " + event4.getAuctionWinner().getLastName() + ", All the bidders: " + event4.getBidders().toString());
    }
}
