# Requirements
See EAuctionApplication.java, I added them there.

# Assumptions and Thought Process
Initially I wrote the program rather quickly to make sure my models and services made sense. I wanted to make sure 
separation of concerns were handled correctly. Due to time, I made the following assumptions:
* No nulls are passed into my service, not tests here, no time!
* No "orchestrator". Due to time, it was easier to kick off timers when initializing the AuctionEvents.
* I have comments everywhere. This isn't how I'd deliver production code but I wanted you to see where my mind was
when I coded this. Thought it might be helpful for the 'whys'.
* No database/persistence layer, again, due to time.

# Output
## (Important) Results of the log file

* 2018-06-07 19:42:01.688  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : Started EAuctionApplication in 1.783 seconds (JVM running for 2.293)
* 2018-06-07 19:42:01.698  INFO 8318 --- [           main] c.m.eAuction.service.AuctionService      : Stopped auction prematurely for Rock n roll ain't noise pollution
* 2018-06-07 19:42:01.698 ERROR 8318 --- [           main] c.m.eAuction.service.AuctionService      : Couldn't find auction STOP drop and roll to cancel!
* 2018-06-07 19:42:01.698  INFO 8318 --- [           main] c.m.eAuction.service.AuctionService      : Stopped auction prematurely for STOP drop and roll
* 2018-06-07 19:42:01.698  INFO 8318 --- [           main] c.m.eAuction.service.AuctionService      : Stopped auction prematurely for Time is ticking away.
* 2018-06-07 19:42:01.698  INFO 8318 --- [           main] c.m.eAuction.service.AuctionService      : Stopped auction prematurely for Look good for cheap!
* 2018-06-07 19:42:01.703  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : com.markremi.eAuction.model.AuctionEvent@349d0836[product=com.markremi.eAuction.model.Product@3e134896,bidders=[Mark Remi, Brian Lenz, Brian Lenz],highestBid=550.0,auctionWinner=Brian Lenz,startingValue=350.0,id=e1c53ed6-7c37-4d62-9028-f78271167f6e,name=Rock n roll ain't noise pollution,start=2018-06-07T19:42:01.697,end=2018-06-07T19:42:01.698]
* 2018-06-07 19:42:01.703  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : com.markremi.eAuction.model.AuctionEvent@4ebadd3d[product=com.markremi.eAuction.model.Product@6ac97b84,bidders=[Ted O'Neill, Bill Cawthra, Brian Lenz],highestBid=5200.0,auctionWinner=Bill Cawthra,startingValue=2488.99,id=79e03129-706c-4bf6-8d1c-9fa80650a186,name=Time is ticking away.,start=2018-06-07T19:42:01.698,end=2018-06-07T19:42:01.698]
* 2018-06-07 19:42:01.703  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : com.markremi.eAuction.model.AuctionEvent@2d0bfb24[product=com.markremi.eAuction.model.Product@c3fa05a,bidders=[Mark Remi],highestBid=34000.32,auctionWinner=Mark Remi,startingValue=25000.0,id=e0182e79-0c2a-4dfc-825c-b1d7315eb8ae,name=Look good for cheap!,start=2018-06-07T19:42:01.698,end=2018-06-07T19:42:01.698]
* 2018-06-07 19:42:01.704  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : com.markremi.eAuction.model.AuctionEvent@7b44b63d[product=com.markremi.eAuction.model.Product@4a699efa,bidders=[],highestBid=15.0,auctionWinner=<null>,startingValue=15.0,id=b6c7b631-2bd7-4b9e-9216-013e69d1d2b1,name=STOP drop and roll,start=2018-06-07T19:42:01.696,end=2018-06-07T20:42:01.696]
* 2018-06-07 19:42:01.704  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : ............
* 2018-06-07 19:42:01.704  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : ............
* 2018-06-07 19:42:01.704  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : ............
* 2018-06-07 19:42:01.704  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : Auction "Rock n roll ain't noise pollution": Starting price: $350.0, Winning Bid : $550.0, Highest Bidder: Brian Lenz, All the bidders: [Mark Remi, Brian Lenz, Brian Lenz]
* 2018-06-07 19:42:01.704  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : Auction "STOP drop and roll": Starting price: $15.0, Winning Bid : $15.0, Highest Bidder: null
* 2018-06-07 19:42:01.704  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : Auction "Time is ticking away.": Starting price: $2488.99, Winning Bid : $5200.0, Highest Bidder: Bill Cawthra, All the bidders: [Ted O'Neill, Bill Cawthra, Brian Lenz]
* 2018-06-07 19:42:01.704  INFO 8318 --- [           main] c.markremi.eAuction.EAuctionApplication  : Auction "Look good for cheap!": Starting price: $25000.0, Winning Bid : $34000.32, Highest Bidder: Mark Remi, All the bidders: [Mark Remi]
